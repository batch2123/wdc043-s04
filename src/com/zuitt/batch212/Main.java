package com.zuitt.batch212;

public class Main {
    public static void main(String[] args) {

        User admin = new User();

        admin.setFirstName("Mark");
        System.out.println("User's first name:\n" + admin.getFirstName());

        admin.setLastName("Santos");
        System.out.println("User's Last name:\n" + admin.getLastName());

        admin.setAddress("Bulacan");
        System.out.println("User's Address:\n" + admin.getAddress());

        admin.setAge(32);
        System.out.println("User's age\n" + admin.getAge());


        Course myCourse = new Course();

        myCourse.setName("Mathematics");
        System.out.println("Course's name:\n" + myCourse.getName());

        myCourse.setDescription("Learn hard computation");
        System.out.println("Course's Description:\n" + myCourse.getDescription());

        myCourse.setSeats(50);
        System.out.println("Course's Seats:\n" + myCourse.getSeats());

        myCourse.setFee(1000);
        System.out.println("Course's fee:\n" + myCourse.getFee());

        System.out.println("Course's instructor's first name:\n" + admin.getFirstName());









    }
}
