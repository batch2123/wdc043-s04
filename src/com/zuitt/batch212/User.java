package com.zuitt.batch212;

public class User {

    private String firstName;

    private String lastName;

    private int age;

    private String address;


//    Constructor

    public User(){};

    public User(String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }


//    Setters

    public void setFirstName(String firstName){this.firstName = firstName;}
    public void setLastName(String lastName){this.lastName = lastName;}
    public void setAge(int age){this.age = age;}
    public void setAddress(String address){this.address = address;}

//    Getters

    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public int getAge(){return age;}
    public String getAddress(){return address;}







}
